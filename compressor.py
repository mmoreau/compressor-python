#!/usr/bin/python3

try:
    import os.path
    from PIL import Image
    from math import log
except:
    pass

class Tools:

    @classmethod
    def humansize(cls, n, binary=False):

        lock = 0

        if isinstance(n, (float, int)):
            if n > 0 and n < 1208925819614629174706176:
                lock += 1

        if isinstance(binary, bool):
            lock += 1

        if lock == 2:
            base = ((1000, ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")), (1024, ("B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB")))[binary]
            indice = int(log(n, base[0]))
            return f"{(n / (base[0] ** indice)):.2f} {base[1][indice]}"


class Compressor:

    @classmethod
    def picture(cls, path, quality=0, dimension=None, format="JPEG", outdir=None):

        errors = []

        if isinstance(path, str):
            if os.path.isfile(path):

                fileext = ".".join(path.split(".")[1:]).lower()

                if fileext in ("jpeg", "jpg", "png"):
                    dirname = os.path.dirname(os.path.abspath(path))
                    filename = os.path.basename(path).split(".")[0]
                else:
                    errors.append("The argument 'path' must have an extension 'jpeg', 'jpg', 'png'")
            else:
                errors.append("The 'path' argument must be a file")

        
        if isinstance(quality, int):
            if not 0 <= quality <= 100:
                errors.append("The 'quality' argument must have the value between 0 and 100")


        if dimension is not None:
            if isinstance(dimension, (list, tuple)):
                if len(dimension) != 2:
                    errors.append("The argument 'dimension' must have two values (width, height)")
            else:
                errors.append("The argument 'dimension' must be of the 'array' or 'tuple' type")


        if isinstance(format, str):

            format = format.lower()

            if format not in ("jpeg", "png"):
                errors.append("The 'format' argument must have either the value 'jpeg' or the value 'png'")


        if outdir is not None:
            if isinstance(outdir, str):
                if os.path.isdir(outdir):
                    if os.access(os.path.dirname(os.path.abspath(outdir)), os.W_OK):
                        dirname = outdir
                    else:
                        errors.append("You do not have write permission on this directory")
                else:
                    errors.append("You must enter a directory for the 'outdir' argument of the function")
            

        if not errors:    
            try:
                with Image.open(path) as im:
                    size = im.size
                    width, height = (dimension if dimension else size)
                    outfile = f"{os.path.join(dirname, filename)}_optimized-{width}x{height}.{fileext}"
                    orientation = (width, height) if size[0] > size[1] else (height, width)
                    im = im.resize(orientation, Image.ANTIALIAS)
                    im = im.convert("RGB")
                    im.save(outfile, format=format, optimize=True, quality=quality, progressive=True, subsampling=2)

                    filesizeold = os.path.getsize(path)
                    filesizenew = os.path.getsize(outfile)
                    ratio = f"{(((filesizeold - filesizenew) / filesizeold) * 100):.2f}"

                    data = {
                        "state": "success",
                        "picture": {
                            "size": {
                                "old": filesizeold,
                                "new": filesizenew,
                                "oldhuman": Tools.humansize(filesizeold),
                                "newhuman": Tools.humansize(filesizenew)
                            },

                            "path": {
                                "old": os.path.abspath(path),
                                "new": outfile
                            },

                            "dimension": {
                                "old": {
                                    "width": size[0],
                                    "height": size[1]
                                },

                                "new": {
                                    "width": width,
                                    "height": height
                                }
                            },

                            "ratio": (float(ratio), f"{ratio} %")
                        }
                    }

                    return data
            except:
                pass

        return {"state": "failed", "errors": errors}


# Examples

#Compressor.picture("myfile.jpg")
#Compressor.picture("myfile.jpg", dimension=(1920, 1200))
#Compressor.picture("myfile.jpg", dimension=(1920, 1200), quality=80)
#Compressor.picture("myfile.jpg", dimension=(1920, 1200), format="png")